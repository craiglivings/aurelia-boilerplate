export class App {
	configureRouter(config,router){
		this.router=router;
		config.map([

			{
				route: ["","list"],
				name:'home',
				moduleId: "movies/list",
				title: "List",
				nav: true
			},

			{
				route: "about",
				moduleId: "about/about",
				title: "About",
				nav :true
			},

			{
				route:"details/:id",
				name: 'details',
				moduleId:"movies/details",
				title: "Details",
				nav :false
			},

			{
				route:"edit/:id",
				name: 'edit',
				moduleId:"movies/edit",
				title: "Edit",
				nav :false
			},

			{
				route:"edit",
				name: 'create',
				moduleId:"movies/edit",
				title: "Create",
				nav :false
			}

			]);
		
		}
	}
