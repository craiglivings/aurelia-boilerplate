import {inject} from "aurelia-framework";
import {MovieData} from "./movieData";
import {Router} from "aurelia-router";
import {ValidationRules,ValidationController} from "aurelia-validation";

@inject(MovieData,Router,ValidationController) 
export class Edit{
constructor(movieData,router,validationController){
		this.movieData=movieData;
		this.router=router;
}

activate (params){
		return this.movieData.getById(params.id)
							.then(movie => this.movie=movie);

	}

save () {
	
	this.movieData.save(this.movie)
					.then(movie => {
						console.log('Movie ID:'+ movie.id);
						let url= this.router.generate("details", {id: movie.id});
						this.router.navigate(url);
					})

}

} 