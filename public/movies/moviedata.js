import {inject} from "aurelia-framework";
import {HttpClient} from "aurelia-http-client";

let baseUrl = "/api";

@inject(HttpClient)
export class MovieData {

	constructor(httpClient){
		this.http = httpClient;
	}

	getAll() {
		return this.http.get(baseUrl+'/movies.json')
						.then(response => {
							return response.content;
						});
	}

	getById(id) {
		return this.http.get(baseUrl+'/'+id+'.json')
						.then(response => {
							console.log(response.content)
							return response.content;
						});
	}

	save(movie) {
		console.log(movie);
		
		var request=this.http.createRequest();
		request.asGet()
			.withUrl(baseUrl)
			.withHeader("Accept","application/json")
			.withHeader("Content-Type","application/json")
			.withContent(movie)
		
		return request.send().then(response => response.content);
	}

}